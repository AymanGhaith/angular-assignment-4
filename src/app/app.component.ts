import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  numbers = [];
  title = 'angular-assignment-four';

  addNewNumber(addednumber: number) {
    this.numbers.push(addednumber);
  }
}

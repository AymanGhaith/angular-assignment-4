import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { GameControlComponent } from './game-control/game-control.component';
import { OddNumbersComponent } from './odd-numbers/odd-numbers.component';
import { EvenNumbersComponent } from './even-numbers/even-numbers.component';

@NgModule({
  declarations: [
    AppComponent,
    GameControlComponent,
    OddNumbersComponent,
    EvenNumbersComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

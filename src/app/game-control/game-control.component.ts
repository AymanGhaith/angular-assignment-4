import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-game-control',
  templateUrl: './game-control.component.html',
  styleUrls: ['./game-control.component.css']
})
export class GameControlComponent implements OnInit {
  counter = 0;
  intervalRef;
  @Output() newIntervalRecieved = new EventEmitter<number>();

  constructor() { }

  ngOnInit() {
  }

  onGameStarted() {
    this.intervalRef = setInterval(() => { this.newIntervalRecieved.emit(++this.counter) }, 1000);
  }

  onGameStopped() {
    clearInterval(this.intervalRef);
  }
}
